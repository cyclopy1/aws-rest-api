import json
import os

import boto3
from boto3.dynamodb.types import TypeDeserializer


dynamo_client = boto3.client('dynamodb')
deserializer = TypeDeserializer()
TABLE_NAME = os.getenv('TABLE_NAME')

def lambda_handler(event, context):
    """Function to retrieve a task."""
    try:
        user_id = event['requestContext']['authorizer']['claims']['sub']
        task_id = event['pathParameters']['taskid']
        response = dynamo_client.get_item(
            TableName=TABLE_NAME,
            Key={'UserID': {'S': user_id}, 'TaskID': {'S': task_id}}
        )
        if 'Item' in response:
            item = response['Item']
            return {
                'statusCode': 200,
                'body': json.dumps(
                    {k: deserializer.deserialize(v) for k, v in item.items()}
                )
            }
        return {
            'statusCode': 404,
            'body': json.dumps({'message': 'Item not found'})
        }
    except KeyError:
        return {
            'statusCode': 400,
            'body': json.dumps({
                'message': 'User ID not found'
            })
        }