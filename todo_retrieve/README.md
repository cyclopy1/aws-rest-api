# **todo_retrieve** lambda function
This function perform a getItem to retrieve a task and return it.

---

## Parameter event of the lambda function

```python
{
    'resource': '/todos/{title}',
    'path': '/todos/Test',
    'httpMethod': 'GET',
    'headers': None,
    'multiValueHeaders': None,
    'queryStringParameters': None,
    'multiValueQueryStringParameters': None,
    'pathParameters': {'title': 'Test'},
    'stageVariables': None,
    'requestContext': {
        'resourceId': 'w3hy66',
        'resourcePath': '/todos/{title}',
        'httpMethod': 'GET',
        'extendedRequestId': 'bkILkFTZDoEFfXA=',
        'requestTime': '02/Mar/2021:15:04:03 +0000',
        'path': '/todos/{title}',
        'accountId': '070931582405',
        'protocol': 'HTTP/1.1',
        'stage': 'test-invoke-stage',
        'domainPrefix': 'testPrefix',
        'requestTimeEpoch': 1614697443569,
        'requestId': '10cde8cb-d331-4644-8a00-48ba6acc1332',
        'identity': {
            'cognitoIdentityPoolId': None,
            'cognitoIdentityId': None,
            'apiKey': 'test-invoke-api-key',
            'principalOrgId': None,
            'cognitoAuthenticationType': None,
            'userArn': 'arn:aws:iam::070931582405:user/MarcAureleCoste',
            'apiKeyId': 'test-invoke-api-key-id',
            'userAgent': 'aws-internal/3 aws-sdk-java/1.11.946 Linux/4.9.230-0.1.ac.223.84.332.metal1.x86_64 OpenJDK_64-Bit_Server_VM/25.282-b08 java/1.8.0_282 vendor/Oracle_Corporation',
            'accountId': '070931582405',
            'caller': 'AIDARBA62BXCTPZJNPSJ2',
            'sourceIp': 'test-invoke-source-ip',
            'accessKey': 'ASIARBA62BXC3KD2ESGY',
            'cognitoAuthenticationProvider': None,
            'user': 'AIDARBA62BXCTPZJNPSJ2'
        },
        'domainName': 'testPrefix.testDomainName',
        'apiId': '703z48diq1'
    },
    'body': None,
    'isBase64Encoded': False
}
```