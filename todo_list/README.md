# **todo_list** lambda function
This function perform a scan on the DynamoDB table to retrieve all the tasks.

---
## Result received from DynamoDB
The results received after a scan of the DynamoDB table should look like this :
```python
{
    'Items': [
        {
            'Finished': {'BOOL': False},
            'Title': {'S': 'Hello World !'}, 
            'Description': {'S': 'Hello World! How are you?'}
        }
    ],
    'Count': 1,
    'ScannedCount': 1,
    'ResponseMetadata': {
        'RequestId': 'UBII78FG2TMG925QFNDBUFJ743VV4KQNSO5AEMVJF66Q9ASUAAJG',
        'HTTPStatusCode': 200,
        'HTTPHeaders': {
            'server': 'Server',
            'date': 'Mon, 01 Mar 2021 15:21:38 GMT',
            'content-type': 'application/x-amz-json-1.0',
            'content-length': '144',
            'connection': 'keep-alive',
            'x-amzn-requestid': 'UBII78FG2TMG925QFNDBUFJ743VV4KQNSO5AEMVJF66Q9ASUAAJG',
            'x-amz-crc32': '1676739880'
        },
        'RetryAttempts': 0
    }
}
```

We extract the "Items" and use a TypeDeserializer (see below) to transform it to a
more exploitable format.

---

## Convert to more "usable" dict

For the moment we get the 'Items' part of the result and we use the `TypeDeserializer` class to convert the low_level format received from DynamoDB to a more usable dict.
```python
from boto3.dynamodb.types import TypeDeserializer

items = [{
    'Finished': {'BOOL': False},
    'Title': {'S': 'Hello World !'}, 
    'Description': {'S': 'Hello World! How are you?'}
}]
deserializer = TypeDeserializer()
final_result = [
    {k: deserializer.deserialize(v) for k, v in item.items()}
    for item in items
]
# Final result now look like this :
# {
#     'Finished': False,
#     'Title': 'Hello World !', 
#     'Description': 'Hello World! How are you?'
# }
```