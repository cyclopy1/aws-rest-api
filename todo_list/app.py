import base64
import json
import os
from typing import Dict, Optional

import boto3
from boto3.dynamodb.types import TypeDeserializer


dynamo_client = boto3.client("dynamodb")
deserializer = TypeDeserializer()
TABLE_NAME = os.getenv("TABLE_NAME")
LIMIT = os.getenv("LIMIT", 5)

QUERY_PARAMS = {
    "TableName": os.getenv("TABLE_NAME"),
    "KeyConditionExpression": "UserID = :userid",
    "Limit": int(os.getenv("LIMIT", 5)),
}


def decode_input_lek(input_lek: str) -> Dict[str, Dict[str, str]]:
    return json.loads(base64.b64decode(input_lek))


def encode_output_lek(output_lek: Dict[str, Dict[str, str]]) -> Optional[str]:
    if output_lek is None:
        return None
    return base64.b64encode(json.dumps(output_lek).encode("utf-8")).decode("utf-8")


def lambda_handler(event, context):
    """List all the tasks for a user.

    We get the 'Items' from the results return by DynamoDB and we use the
    `TypeDeserializer` to transform the items to a more comprehensive list of
    dictionaries.
    Then we return this list in the body of the response.
    """
    try:
        user_id = event["requestContext"]["authorizer"]["claims"]["sub"]
        QUERY_PARAMS["ExpressionAttributeValues"] = {":userid": {"S": user_id}}

        if event["queryStringParameters"] is not None:
            input_lek = event["queryStringParameters"].get("lek")
            if input_lek is not None:
                QUERY_PARAMS["ExclusiveStartKey"] = decode_input_lek(input_lek)

        response = dynamo_client.query(**QUERY_PARAMS)
        items = response.get("Items", [])
        output_lek = response.get("LastEvaluatedKey", None)

        task_list = [
            {k: deserializer.deserialize(v) for k, v in item.items()} for item in items
        ]
        return {
            "statusCode": 200,
            "body": json.dumps(
                {
                    "count": len(task_list),
                    "results": task_list,
                    "last_evaluated_key": encode_output_lek(output_lek),
                }
            ),
        }
    except KeyError:
        return {
            "statusCode": 400,
            "body": json.dumps(
                {"message": "User ID not found this should never happened"}
            ),
        }
