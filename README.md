# aws-rest-api

An API made with API Gateway to manage tasks. In this project you will find the SAM template for the deploiement, the source code for the Lambda functions, and also script to interract with Cognito because the is no clients for to register and login with it.

## Deploy resources
❗I cannot certify that the deploiement of the resources will be at no cost❗

The following resources will be deployed in your AWS account :
- 1 DynamoDB table
- 1 Cognito UserPool
- 1 Cognito Application Client
- 1 API Gateway
- 5 Lambda Functions

To deploy the application you have to use SAM CLI, you can found informations on how to install it [here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).

When installed you can run the following commands:
```bash
sam build
sam deploy  # Add --guided if it's the firt time you deploy the application
```

Here some informations from the original version of the **README** that can be usefull for your first launch.
  > * **Stack Name**: The name of the stack to deploy to CloudFormation. This should be unique to your account and region, and a good starting point would be something matching your project name.
  > * **AWS Region**: The AWS region you want to deploy your app to.
  > * **Confirm changes before deploy**: If set to yes, any change sets will be shown to you before execution for manual review. If set to no, the AWS SAM CLI will automatically deploy application changes.
  > * **Allow SAM CLI IAM role creation**: Many AWS SAM templates, including this example, create AWS IAM roles required for the AWS Lambda function(s) included to access AWS services. By default, these are scoped down to minimum required permissions. To deploy an AWS CloudFormation stack which creates or modified IAM roles, the `CAPABILITY_IAM` value for `capabilities` must be provided. If permission isn't provided through this prompt, to deploy this example you must explicitly pass `--capabilities CAPABILITY_IAM` to the `sam deploy` command.
  > * **Save arguments to samconfig.toml**: If set to yes, your choices will be saved to a configuration file inside the project, so that in the future you can just re-run `sam deploy` without parameters to deploy changes to your application.

You can find your API Gateway Endpoint URL in the output values displayed after deployment.

## Create a user in Cognito
At the root of the project there is a folder called **cognito_scripts** with two scripts *register.py* and *login.py*. To use these script you first have to retrieve the application "**App client id**" in the AWS console.

![cognito_application_client_id](https://i.ibb.co/JBVcQPx/cognito-application-client-id.png)

You can then do the following :
```bash
export COGNITO_CLIENT_ID=<App client id>
export COGNITO_USERNAME=<User name>
export COGNITO_PASSWORD=<User password>
python register.py
python login.py
# output
> AccessToken: <My super long access token>
  IdToken: <My super long id token>
  RefreshToken: <My extra long refresh token>
```

You can now send request to the API using the **IdToken** in your authorization header.

(Example for Postman)
![postman_bearer_token_authorization](https://i.ibb.co/HY1VCkD/postman-bearer-token-authorization.png)

## Cleanup

You can delete the resources you deploy using the SAM CLI simply run the following :
```bash
aws cloudformation delete-stack --stack-name <Your Stack Name>
```
