# **todo_update** lambda function
This function perform an updateItem to update a task. A check is made to be sure the task exist before the update.

---

## Response received from DynamoDB after the update
```python
{
    'Attributes': {
        'Finished': {'BOOL': False},
        'Title': {'S': 'Test'},
        'Description': {'S': 'This is a description updated'}
    },
    'ResponseMetadata': {
        'RequestId': 'PRDNMSB4QIMFQCSTH1U48F0GLNVV4KQNSO5AEMVJF66Q9ASUAAJG',
        'HTTPStatusCode': 200,
        'HTTPHeaders': {
            'server': 'Server',
            'date': 'Wed, 03 Mar 2021 11:10:16 GMT',
            'content-type': 'application/x-amz-json-1.0',
            'content-length': '115',
            'connection': 'keep-alive',
            'x-amzn-requestid': 'PRDNMSB4QIMFQCSTH1U48F0GLNVV4KQNSO5AEMVJF66Q9ASUAAJG',
            'x-amz-crc32': '1056713922'
        },
        'RetryAttempts': 0
    }
}
```
The *Attributes* element is the representation of our updated object. It's return by DynamoDB because we use the `ReturnValues='ALL_NEW'` when calling the *update_item* method.

We have to transform this *Attributes* to a more understandable dictionary (with a **TypeDeserializer**) and return it to the client.