import dataclasses
import json
import os
from typing import List, Optional, Tuple

import boto3
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer


dynamo_client = boto3.client('dynamodb')
serializer = TypeSerializer()
deserializer = TypeDeserializer()
TABLE_NAME = os.getenv('TABLE_NAME')

@dataclasses.dataclass
class Task:
    """Representation of task."""
    Title: Optional[str] = None
    Description: Optional[str] = None
    Finished: Optional[bool] = None

    def to_dynamo(self) -> Tuple[str, dict]:
        """Return the 'update_expression' and 'expression_attribute_values'.

        These two elements can be use to update an item in dynamo db. 
        
        The update_expression is a string explaining the operations to do on the
        differents attributes.
        
        The expression_attribute_values is a dictionary with as key the element
        to replace in the update_expression and as value the value to use for
        the replacement.
        """
        update_expression_parts: List[str] = []
        expression_attribute_values = {}

        for key, value in self.__dict__.items():
            if value:
                token = f':{key.lower()}'
                update_expression_parts.append(f'{key} = {token}')
                expression_attribute_values[token] = serializer.serialize(value)
        return (
            f'SET {", ".join(update_expression_parts)}',
            expression_attribute_values
        )


def lambda_handler(event, context):
    """Function to update a task."""
    try:
        user_id = event['requestContext']['authorizer']['claims']['sub']
        task_id: str = event['pathParameters']['taskid']
        body = json.loads(event.get('body', {}))
    
        tdata = Task(**{
            k: v for k, v in body.items()
            if k in set(f.name for f in dataclasses.fields(Task))
        })
        update_expression, expression_attribute_values = tdata.to_dynamo()
        response = dynamo_client.update_item(
            TableName=TABLE_NAME,
            Key={'UserID': {'S': user_id}, 'TaskID': {'S': task_id}},
            ConditionExpression='attribute_exists(UserID) AND attribute_exists(TaskID)',
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_attribute_values,
            ReturnValues='ALL_NEW'
        )
        updated_element = response['Attributes']
        return {
            'statusCode': 200,
            'body': json.dumps({
                k: deserializer.deserialize(v) for k, v in updated_element.items()
            })
        }
    except dynamo_client.exceptions.ConditionalCheckFailedException:
        return {
            'statusCode': 404,
            'body': json.dumps({'message': 'Not found'})
        }
    except KeyError:
        return {
            'statusCode': 400,
            'body': json.dumps({
                'message': 'User ID not found'
            })
        }
