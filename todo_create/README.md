# **todo_create** lambda function
This function perform a putItem in the DynamoDB table to create a new task.

---

## Test body
The following JSON can be used to create a 'test' task.
```json
{
    "Title": "Test",
    "Description": "This is a test"
}
```

---

## Parameter event of the lambda function
```python
{
    'resource': '/todos', 
    'path': '/todos',
    'httpMethod': 'POST',
    'headers': None,
    'multiValueHeaders': None,
    'queryStringParameters': None,
    'multiValueQueryStringParameters': None,
    'pathParameters': None,
    'stageVariables': None,
    'requestContext': {
        'resourceId': '0bqeny',
        'resourcePath': '/todos',
        'httpMethod': 'POST',
        'extendedRequestId': 'bg_haGwYDoEFcpA=',
        'requestTime': '01/Mar/2021:16:14:13 +0000',
        'path': '/todos',
        'accountId': '070931582405',
        'protocol': 'HTTP/1.1',
        'stage': 'test-invoke-stage',
        'domainPrefix': 'testPrefix',
        'requestTimeEpoch': 1614615253792,
        'requestId': '8a88f072-578e-4c51-92b6-ace1a1afee32',
        'identity': {
            'cognitoIdentityPoolId': None,
            'cognitoIdentityId': None,
            'apiKey': 'test-invoke-api-key',
            'principalOrgId': None,
            'cognitoAuthenticationType': None,
            'userArn': 'arn:aws:iam::070931582405:user/MarcAureleCoste',
            'apiKeyId': 'test-invoke-api-key-id',
            'userAgent': 'aws-internal/3 aws-sdk-java/1.11.946 Linux/4.9.230-0.1.ac.223.84.332.metal1.x86_64 OpenJDK_64-Bit_Server_VM/25.282-b08 java/1.8.0_282 vendor/Oracle_Corporation',
            'accountId': '070931582405',
            'caller': 'AIDARBA62BXCTPZJNPSJ2',
            'sourceIp': 'test-invoke-source-ip',
            'accessKey': 'ASIARBA62BXCZCYD3CW4',
            'cognitoAuthenticationProvider': None,
            'user': 'AIDARBA62BXCTPZJNPSJ2'
        },
        'domainName': 'testPrefix.testDomainName',
        'apiId': '703z48diq1'
    },
    'body': '{\n    "Title": "HEY",\n    "Description": "YO"\n}',
    'isBase64Encoded': False
}
```