import json
import os
import uuid

import boto3
from boto3.dynamodb.types import TypeSerializer


dynamo_client = boto3.client('dynamodb')
serializer = TypeSerializer()
TABLE_NAME = os.getenv('TABLE_NAME')

def lambda_handler(event, context):
    """Function to create a task."""
    try:
        user_id = event['requestContext']['authorizer']['claims']['sub']
        body = json.loads(event.get('body', {}))
        body['UserID'] = user_id
        body['TaskID'] = str(uuid.uuid4())
        if len(body) == 4 and 'Title' in body and 'Description' in body:
            body['Finished'] = False
            dynamo_client.put_item(
                TableName=TABLE_NAME,
                Item={k: serializer.serialize(v) for k, v in body.items()}
            )
            return {
                'statusCode': 200,
                'body': json.dumps(body)
            }
        return {
            'statusCode': 400,
            'body': json.dumps({
                'message': 'Bad request'
            })
        }
    except KeyError:
        return {
            'statusCode': 400,
            'body': json.dumps({
                'message': 'User ID not found'
            })
        }
