import json
import os

import boto3


dynamo_client = boto3.client('dynamodb')
TABLE_NAME = os.getenv('TABLE_NAME')

def lambda_handler(event, context):
    """Function to delete a task."""
    try:
        user_id = event['requestContext']['authorizer']['claims']['sub']
        task_id = event['pathParameters']['taskid']
        condition_expression = f"attribute_exists(UserID) AND attribute_exists(TaskID)"
        dynamo_client.delete_item(
            TableName=TABLE_NAME,
            Key={'UserID': {'S': user_id}, 'TaskID': {'S': task_id}},
            ConditionExpression=condition_expression
        )
        return {
            'statusCode': 200,
            'body': json.dumps({'message': 'Task deleted'})
        }
    except dynamo_client.exceptions.ConditionalCheckFailedException:
        return {
            'statusCode': 404,
            'body': json.dumps({'message': 'Not found'})
        }
    except KeyError:
        return {
            'statusCode': 400,
            'body': json.dumps({
                'message': 'User ID not found'
            })
        }
