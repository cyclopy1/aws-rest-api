"""A script to register a new user.

To launch this script you have to set three environment variables:
- COGNITO_CLIENT_ID
- COGNITO_USERNAME
- COGNITO_PASSWORD
"""
import os

import boto3


boto3.setup_default_session(profile_name='me', region_name='eu-west-1')
cog_client = boto3.client('cognito-idp')

def register(client_id: str, username: str, password: str):
    cog_client.sign_up(ClientId=client_id,Username=username,Password=password)


if __name__ == '__main__':
    client_id = os.environ.get('COGNITO_CLIENT_ID')
    username = os.environ.get('COGNITO_USERNAME')
    password = os.environ.get('COGNITO_PASSWORD')
    register(client_id, username, password)
