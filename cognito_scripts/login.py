"""Script to login as user and retreive auth token.

To launch this script you have to set three environment variables:
- COGNITO_CLIENT_ID
- COGNITO_USERNAME
- COGNITO_PASSWORD
"""
import os

import boto3


boto3.setup_default_session(profile_name='me', region_name='eu-west-1')
cog_client = boto3.client('cognito-idp')

def auth(client_id: str, username: str, password: str):
    '''Initializes a cognito user in clientId specified'''
    response = cog_client.initiate_auth(
        ClientId=client_id,
        AuthFlow='USER_PASSWORD_AUTH',
        AuthParameters={'USERNAME': username,'PASSWORD': password}
    )
    if response:
        tokens = response['AuthenticationResult']
        print(f"AccessToken: {tokens['AccessToken']}")
        print(f"IdToken: {tokens['IdToken']}")
        print(f"RefreshToken: {tokens['RefreshToken']}")
    else:
        print('Authentication failed')

if __name__ == '__main__':
    client_id = os.environ.get('COGNITO_CLIENT_ID')
    username = os.environ.get('COGNITO_USERNAME')
    password = os.environ.get('COGNITO_PASSWORD')
    auth(client_id, username, password)